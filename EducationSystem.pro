# Created by and for Qt Creator This file was created for editing the project sources only.
# You may attempt to use it for building too, by modifying this file here.

#TARGET = EducationSystem

QT = core gui widgets

HEADERS = \
   $$PWD/hps/src/basic_type/basic_type.h \
   $$PWD/hps/src/basic_type/float_serializer.h \
   $$PWD/hps/src/basic_type/int_serializer.h \
   $$PWD/hps/src/basic_type/string_serializer.h \
   $$PWD/hps/src/basic_type/uint_serializer.h \
   $$PWD/hps/src/buffer/buffer.h \
   $$PWD/hps/src/buffer/char_array_input_buffer.h \
   $$PWD/hps/src/buffer/stream_input_buffer.h \
   $$PWD/hps/src/buffer/stream_output_buffer.h \
   $$PWD/hps/src/buffer/string_input_buffer.h \
   $$PWD/hps/src/buffer/string_output_buffer.h \
   $$PWD/hps/src/container/array_serializer.h \
   $$PWD/hps/src/container/bit_filters.h \
   $$PWD/hps/src/container/container.h \
   $$PWD/hps/src/container/deque_serializer.h \
   $$PWD/hps/src/container/is_unique_ptr.h \
   $$PWD/hps/src/container/list_serializer.h \
   $$PWD/hps/src/container/map_serializer.h \
   $$PWD/hps/src/container/pair_serializer.h \
   $$PWD/hps/src/container/set_serializer.h \
   $$PWD/hps/src/container/unique_ptr_serializer.h \
   $$PWD/hps/src/container/unordered_map_serializer.h \
   $$PWD/hps/src/container/unordered_set_serializer.h \
   $$PWD/hps/src/container/vector_serializer.h \
   $$PWD/hps/src/hps.h \
   $$PWD/hps/src/serializer.h \
   $$PWD/src/institute.h \
   $$PWD/src/person.h \
   $$PWD/src/string_reader.h

SOURCES = \
   $$PWD/hps/example/basic.cc \
   $$PWD/hps/example/custom_type.cc \
   $$PWD/hps/example/file_stream.cc \
   $$PWD/hps/src/basic_type/float_serializer_test.cc \
   $$PWD/hps/src/basic_type/int_serializer_test.cc \
   $$PWD/hps/src/basic_type/string_serializer_test.cc \
   $$PWD/hps/src/basic_type/uint_serializer_test.cc \
   $$PWD/hps/src/benchmark/double_array_test.cc \
   $$PWD/hps/src/benchmark/map_test.cc \
   $$PWD/hps/src/benchmark/sparse_matrix_test.cc \
   $$PWD/hps/src/container/array_serializer_test.cc \
   $$PWD/hps/src/container/deque_serializer_test.cc \
   $$PWD/hps/src/container/list_serializer_test.cc \
   $$PWD/hps/src/container/map_serializer_test.cc \
   $$PWD/hps/src/container/pair_serializer_test.cc \
   $$PWD/hps/src/container/set_serializer_test.cc \
   $$PWD/hps/src/container/unique_ptr_serializer_test.cc \
   $$PWD/hps/src/container/unordered_map_serializer_test.cc \
   $$PWD/hps/src/container/unordered_set_serializer_test.cc \
   $$PWD/hps/src/container/vector_serializer_test.cc \
   $$PWD/hps/src/hps_test.cc \
   $$PWD/src/institute.cpp \
   $$PWD/src/main.cpp \
   $$PWD/src/person.cpp \
   $$PWD/src/string_reader.cpp

INCLUDEPATH = \
    $$PWD/hps/src \
    $$PWD/hps/src/basic_type \
    $$PWD/hps/src/buffer \
    $$PWD/hps/src/container \
    $$PWD/src

#DEFINES = 

