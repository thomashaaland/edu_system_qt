# Education System
This is a set of classes which together makes an education system. In front there is a simple interface which let's you create new faculties, courses, students and teachers. 

## Requirements
Requires the HPS library. It can be found (here)[https://github.com/jl2922/hps]. Also requires the g++ to be installed. To install g++ on ubuntu press `C-t` to open terminal and type `sudo apt-get install g++` in the window. To install g++ in windows follow (these)[https://genome.sph.umich.edu/wiki/installing_MinGW_%26_MSYS_on_windows] instructions.

## Installation
To install from root run `/bin/make`.

## Running
To run type from root `/bin/make run`. If this is the first time you run this program the program will let you create an instute, a course with a teacher and any number of students attached to that course. You can create any number of institutes with any number of courses. The program will save this to disk on the file `data/data_file.txt`. The next time you run the program the previous data will be loaded and you get an option to view it. 

## Issues
Currently there is no type guards in the menu system so typing something not requested will crash the program. 
