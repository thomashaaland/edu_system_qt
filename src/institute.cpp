#include <iostream>
#include "institute.h"

//--------------------------------------//
// Course related things
Course::Course() {}
Course::Course(QString name) : course_name(name) {}

void Course::change_curriculum(QString curriculum)
{
    this->curriculum = curriculum;
}

void Course::assign_teacher(int teacher_id)
{
    teacher_by_id = teacher_id;
}

void Course::enroll_student(int student_id)
{
    enrolled_students_by_id.emplace_back(student_id);
}

int Course::remove_student(int student_id)
{
    for (size_t iter = 0; iter < enrolled_students_by_id.size(); iter++)
    {
        if (student_id == enrolled_students_by_id[iter])
        {
            enrolled_students_by_id.erase(enrolled_students_by_id.begin() + iter);
            return student_id;
        }
    }
    return -1;
}

int Course::remove_teacher()
{
    int temp_id = teacher_by_id;
    teacher_by_id = -1;
    return temp_id;
}

QString Course::get_course_name()
{
    return course_name;
}

std::vector<int> Course::get_enrolled_students()
{
    return enrolled_students_by_id;
}

int Course::get_teacher_by_id()
{
    return teacher_by_id;
}

//--------------------------------------//
// Faculty related things
Faculty::Faculty() {}
Faculty::Faculty(QString fac_name) : faculty_name(fac_name)
{
    current_id = 0;
}

// Faculty related things
Faculty::Faculty(QString fac_name, int set_id) : faculty_name(fac_name), current_id(set_id) { }


// Destructor
Faculty::~Faculty()
{
    for (auto teacher : hire_pool)
    {
        delete teacher;
    }
    for (auto student : registered_students)
    {
        delete student;
    }
    for (auto course : courses)
    {
        delete course;
    }
}

// Gets the name of the faculty
QString Faculty::get_name()
{
    return this->faculty_name;
}

int Faculty::hire_teacher(QString first_name, QString second_name)
{
    current_id++;
    Teacher *new_teacher = new Teacher(current_id, first_name, second_name);
    hire_pool.emplace_back(new_teacher);
    return current_id;
}

int Faculty::fire_teacher(int teacher_id)
{
    Teacher *temp_teacher = get_teacher(teacher_id);
    if (temp_teacher)
    {
        for (size_t iter = 0; iter < hire_pool.size(); iter++)
        {
            if (teacher_id == hire_pool[iter]->get_id())
            {
                hire_pool.erase(hire_pool.begin() + iter);
            }
        }
        delete temp_teacher;
        return 0;
    }
    return -1;
}

void Faculty::assign_teacher_to_course(int teacher_id, QString course_name)
{
    Course *course = get_course(course_name);
    Teacher *teacher = get_teacher(teacher_id);
    course->assign_teacher(teacher_id);
    teacher->assign_course(course_name);
}

Teacher *Faculty::get_teacher(int teacher_id)
{
    for (Teacher *teacher : hire_pool)
    {
        if (teacher->get_id() == teacher_id) return teacher;
    }
    return nullptr;
}

int Faculty::get_teacher_id_by_name(QString fname, QString lname)
{
    for (Teacher *teacher_ptr : hire_pool)
    {
        if (teacher_ptr->get_name() == fname + " " + lname)
        return teacher_ptr->get_id();
    }
    return -1;
}

int Faculty::register_student(QString firstname, QString secondname)
{
    current_id++;
    Student *new_student = new Student(current_id, firstname, secondname);
    registered_students.emplace_back(new_student);
    return current_id;
}

void Faculty::enroll_student_to_course(int student_id, QString course_name)
{
    Student *student = get_student(student_id);
    Course *course = get_course(course_name);
    course->enroll_student(student_id);
    student->enroll_course(course_name);
}

void Faculty::remove_student_from_course(int student_id, QString course_name)
{
    Student *student = get_student(student_id);
    Course *course = get_course(course_name);
    student->remove_course(course_name);
    course->remove_student(student_id);
}

int Faculty::remove_student(int student_id)
{
    Student *temp_student = get_student(student_id);
    Course *course_ptr;
    for (QString course : temp_student->get_enrolled_courses())
    {
        course_ptr = get_course(course);
        course_ptr->remove_student(student_id);
    }
    for (size_t iter = 0; iter < registered_students.size(); iter++)
    {
        registered_students.erase(registered_students.begin() + iter);
        delete temp_student;
        return 0;
    }
    return -1;
}

Student *Faculty::get_student(int student_id)
{
    for (Student *student : registered_students)
    {
        if (student->get_id() == student_id)
        {
            return student;
        }
    }
    return nullptr;
}

// Create a course
void Faculty::create_course(QString course)
{
    Course *new_course = new Course(course);
    courses.push_back(new_course);
}

int Faculty::remove_course(QString course)
{
    Course *course_ptr = get_course(course);
    int teacher_id = course_ptr->get_teacher_by_id();
    Teacher *teacher_ptr;
    teacher_ptr = get_teacher(teacher_id);
    if (teacher_ptr)
    {
        teacher_ptr->assign_course("none");
    }
    Student *student_ptr;
    for (int student_id : course_ptr->get_enrolled_students())
    {
        student_ptr = get_student(student_id);
        student_ptr->remove_course(course);
    }
    for (size_t iter = 0; iter < courses.size(); iter++)
    {
        if (courses[iter]->get_course_name().compare(course) == 0)
        {
            courses.erase(courses.begin() + iter);
            delete course_ptr;
            return 0;
        }
    }
    return -1;
}

Course *Faculty::get_course(QString course)
{
    for (Course *course_ptr : courses)
    {
        if (course_ptr->get_course_name().compare(course) == 0)
        {
            return course_ptr;
        }
    }
    return nullptr;
}

std::vector<Course*> Faculty::get_all_courses()
{
    return courses;
}

std::vector<Teacher*> Faculty::get_all_employees()
{
    return hire_pool;
}

std::vector<Student*> Faculty::get_all_enlisted_students()
{
    return registered_students;
}

// Function fot describing the faculty.
QString Faculty::describe_faculty()
{
    std::stringstream ss;
    ss << "The faculty: " << this->get_name().toStdString() << std::endl;
    ss << "Running the courses: " << std::endl;
    for (auto course : this->get_all_courses())
    {
        ss << course->get_course_name().toStdString() << std::endl;
        ss << " Taught by: " << get_teacher(course->get_teacher_by_id())->get_name().toStdString() << std::endl;
        ss << "  Enrolled students:" << std::endl;
        for (auto student_id : course->get_enrolled_students())
        {
            ss << "   " << get_student(student_id)->get_name().toStdString() << std::endl;
        }
    }
    return QString::fromStdString(ss.str());
}
