#ifndef INSTITUTE_H
#define INSTITUTE_H

#include <string>
#include <sstream>
#include <QApplication>
#include "person.h"

class Course
{
private:
    QString course_name;
    int teacher_by_id;
    QString curriculum;
    std::vector<int> enrolled_students_by_id;
public:
    Course();
    Course(QString course_name);
    void change_curriculum(QString);
    void enroll_student(int student_id);
    int remove_student(int student_id);
    void assign_teacher(int teacher_id);
    int remove_teacher();

    QString get_course_name();
    std::vector<int> get_enrolled_students();
    int get_teacher_by_id();

    template <class B>
    void serialize(B &buf) const
    {
        buf << course_name << teacher_by_id << curriculum << enrolled_students_by_id;
    }

    template <class B>
    void parse(B &buf)
    {
        buf >> course_name >> teacher_by_id >> curriculum >> enrolled_students_by_id;
    }

};

// This class is responsible for administrative tasks
class Faculty
{
private:
    int current_id;
    QString faculty_name;
    std::vector<Teacher*> hire_pool;
    std::vector<Student*> registered_students;
    std::vector<Course*> courses;
public:
    Faculty();
    Faculty(QString fac_name);
    Faculty(QString fac_name, int set_id);
    ~Faculty();

    QString get_name();

    int hire_teacher(QString first_name, QString second_name);
    int fire_teacher(int teacher_id);
    void assign_teacher_to_course(int teacher_id, QString course);
    Teacher *get_teacher(int teacher_id);
    int get_teacher_id_by_name(QString fname, QString lname);

    int register_student(QString first_name, QString second_name);
    void enroll_student_to_course(int student_id, QString course_name);
    void remove_student_from_course(int student_id, QString course_name);
    int remove_student(int student_id);
    Student *get_student(int student_id);

    void create_course(QString course_name);
    int remove_course(QString course_name);
    Course *get_course(QString course_name);

    std::vector<Course*> get_all_courses();
    std::vector<Teacher*> get_all_employees();
    std::vector<Student*> get_all_enlisted_students();

    // Output
    QString describe_faculty();

    template <class B>
    void serialize(B &buf) const
    {
        std::vector<Teacher> hire_pool_temp;
        std::vector<Student> registered_students_temp;
        std::vector<Course> courses_temp;

        for (auto hire : hire_pool)
        {
            hire_pool_temp.emplace_back(*hire);
        }
        for (auto student : registered_students)
        {
            registered_students_temp.emplace_back(*student);
        }
        for (auto course : courses)
        {
            courses_temp.emplace_back(*course);
        }

        buf << current_id << faculty_name << hire_pool_temp << registered_students_temp << courses_temp;
    }

    template <class B>
    void parse(B &buf)
    {
        std::vector<Teacher> hire_pool_temp;
        std::vector<Student> registered_students_temp;
        std::vector<Course> courses_temp;

        buf >> current_id >> faculty_name >> hire_pool_temp >> registered_students_temp >> courses_temp;

        for (const Teacher hire : hire_pool_temp)
        {
            Teacher *new_teacher = new Teacher(hire);
            hire_pool.emplace_back(new_teacher);
        }
        for (const Student student : registered_students_temp)
        {
            Student *new_student = new Student(student);
            registered_students.emplace_back(new_student);
        }
        for (const Course course : courses_temp)
        {
            Course *new_course = new Course(course);
            courses.emplace_back(new_course);
        }

    }
};

#endif
