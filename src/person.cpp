#include "person.h"

// Person class. Holds info about a person
Person::Person() {}
Person::Person(QString fname, QString lname) : firstname(fname), lastname(lname)
{ }
Person::Person(int id_number, QString fname, QString lname) : id(id_number), firstname(fname), lastname(lname)
{ }

// Getting the name of the person
QString Person::get_name()
{
    return firstname + " " + lastname;
}

// Getting the id of a person
int Person::get_id()
{
    return id;
}

// Teacher. The teacher has some additional functionality related to teaching
Teacher::Teacher() {}
Teacher::Teacher(QString fname, QString lname) : Person(fname, lname)
{ }

// Teacher. The teacher has some additional functionality related to teaching
Teacher::Teacher(int id, QString fname, QString lname) : Person(id, fname, lname)
{ }

// Let's the teacher accept a course
void Teacher::assign_course(QString course)
{
    teaching_courses = course;
}

// Get the course the teacher is teaching
QString Teacher::get_course_taught()
{
    return teaching_courses;
}

// ---------------------------------- //
// Student class
Student::Student() {}
Student::Student(QString fname, QString lname) : Person(fname, lname)
{
    attended_courses.reserve(3);
}

// Student class
Student::Student(int id, QString fname, QString lname) : Person(id, fname, lname)
{
    attended_courses.reserve(3);
}


// The student can enlist to courses
void Student::enroll_course(QString course)
{
    attended_courses.push_back(course);
}

// Method for removing a student from the course
QString Student::remove_course(QString course)
{
    for (size_t iter = 0; iter < attended_courses.size(); iter++)
    {
        if (attended_courses[iter].compare(course) == 0)
        {
            attended_courses.erase(attended_courses.begin() + iter);
            return course;
        }
    }
    return "none";
}

// Method for returning all the courses the student has enrolled onto
std::vector<QString> Student::get_enrolled_courses()
{
    return this->attended_courses;
}
