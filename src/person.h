#ifndef PERSON_H
#define PERSON_H

#include <string>
#include <QApplication>
#include <vector>

class Person
{
protected:
    int id;
    QString firstname;
    QString lastname;

public:
    Person();
    Person(QString fname, QString lname);
    Person(int id, QString fname, QString lname);

    QString get_name();
    void set_id(int id);
    int get_id();

    // Serializing
    template <class B>
    void serialize(B &buf) const
    {
        buf << id << firstname << lastname;
    }

    // Unserializing
    template <class B>
    void parse(B &buf)
    {
        buf >> id >> firstname >> lastname;
    }
};

class Student : public Person
{
private:
    std::vector<QString> attended_courses;

public:
    Student();
    Student(QString fname, QString lname);
    Student(int id, QString fname, QString lname);
    void enroll_course(QString course);
    QString remove_course(QString course);
    std::vector<QString> get_enrolled_courses();

    template <class B>
    void serialize(B &buf) const
    {
        buf << id << firstname << lastname << attended_courses;
    }

    template <class B>
    void parse(B &buf)
    {
        buf >> id >> firstname >> lastname >> attended_courses;
    }
};

class Teacher : public Person
{
private:
    QString teaching_courses;

public:
    Teacher();
    Teacher(QString fname, QString lname);
    Teacher(int id, QString fname, QString lname);
    void assign_course(QString);
    QString get_course_taught();

    template <class B>
    void serialize(B &buf) const
    {
        buf << id << firstname << lastname << teaching_courses;
    }

    template <class B>
    void parse(B &buf)
    {
        buf >> id >> firstname >> lastname >> teaching_courses;
    }
};

#endif
