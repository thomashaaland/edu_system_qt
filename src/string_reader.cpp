#include <QtDebug>
#include "string_reader.h"


// Function for getting text command from terminal
QString get_command()
{
    std::string raw_string;
    std::getline(std::cin, raw_string);
    return QString::fromStdString(raw_string);
}

// Creates a faculty and returns its pointer.
// Wrapped in a function to better facilitate testing
Faculty *create_faculty(QString line)
{
    Faculty *new_faculty = new Faculty(line);
    return new_faculty;
}

// For parsing names of persons
std::pair<QString, QString> parse_person_name(QString line)
{
    std::vector<QString> arguments;
    std::string token;
    std::istringstream sstream(line.toStdString());
    while (std::getline(sstream, token, ' '))
    {
        arguments.emplace_back(token);
    }
    if (arguments.size() != 2)
        throw "Error: You need a first name and a second name [first_name second_name]";
    return std::pair(arguments[0], arguments[1]);
}

// Function to run through the completion of a newly created faculty
// Let's the user create courses teachers and students.
void complete_faculty_fill_in(Faculty *fac)
{
    qDebug() << "How many courses is running? ";
    int number_of_courses = get_command().toInt();
    QString line;
    while (number_of_courses--)
    {
        qDebug() << "Enter a course name [Course]: ";
        line = get_command();
        if (line.compare("n") == 0)
            break;
        fac->create_course(line);
    }
    for (auto course : fac->get_all_courses())
    {
        qDebug() << "Enter a teacher for " << course->get_course_name();
        qDebug() << " [First_name Second_name]: ";
        line = get_command();
        std::pair<QString, QString> teacher_name = parse_person_name(line);
        int teacher_id = fac->hire_teacher(teacher_name.first, teacher_name.second);
        fac->assign_teacher_to_course(teacher_id, course->get_course_name());
    }
    for (auto course : fac->get_all_courses())
    {
        qDebug() << "How many students have enrolled to " << course->get_course_name() << "?: ";
        int number_of_students = get_command().toInt();
        while (number_of_students--)
        {
            qDebug() << "Enter a student for " << course->get_course_name() << " [First_name Second_name]: ";
            line = get_command();
            if (line.compare("n") == 0)
                break;
            std::pair<QString, QString> student_name = parse_person_name(line);
            int student_id = fac->register_student(student_name.first, student_name.second);
            fac->enroll_student_to_course(student_id, course->get_course_name());
        }
    }
}

// Function for writing to file. It serializes data and
// writes to a preset file
void save_data(std::vector<Faculty *> &facs)
{

    // QString serialized = sstream.str();
    std::vector<Faculty> data;
    for (auto fac : facs)
    {
        data.emplace_back(*fac);
    }

    std::ofstream data_file;
    data_file.open("../data/data_file.txt", std::ofstream::binary);
    if (data_file.is_open())
    {
        hps::to_stream(data, data_file);
    }
    else
    {
        qDebug() << "Failure to open file" ;
    }
    data_file.close();
}

// Reader
std::vector<Faculty *> load_save_data()
{
    std::vector<Faculty *> faculties;
    std::vector<Faculty> parsed_data;

    QString filename = "../data/data_file.txt";
    QString line;
    std::ifstream data_file;
    data_file.open(filename.toStdString(), std::ifstream::binary);
    if (data_file.is_open())
    {
        parsed_data = hps::from_stream<std::vector<Faculty>>(data_file);
    }
    else
    {
        qDebug() << "Could not open file " << filename ;
    }
    data_file.close();

    for (Faculty fac : parsed_data)
    {
        Faculty *new_faculty = new Faculty(fac);
        faculties.emplace_back(new_faculty);
    }

    return faculties;
}

// The menu_option_create_faculty starts from the top and adds everything you need to add a
// complete faculty.
void menu_option_create_faculty(std::vector<Faculty *> &faculty_list)
{
    qDebug() << "-----------------------------------------------" ;
    qDebug() << "String reader up and running! Press 'q' to quit" ;
    QString line;
    while (true)
    {
        qDebug() << "Enter a faculty on the format: [Faculty name]: ";
        line = get_command();
        if (line.compare("q") == 0)
            break;
        Faculty *temp = create_faculty(line);
        complete_faculty_fill_in(temp);
        faculty_list.emplace_back(temp);
    }
}

void menu_option_create_new_course(Faculty *faculty)
{
    QString course_name;
    std::pair<QString, QString> teacher_name;

    qDebug() << "** Creating a new course for " << faculty->get_name() << " ***" ;
    qDebug() << "Enter a course name [Course]: ";
    course_name = get_command();
    faculty->create_course(course_name);
    qDebug() << "We will need to hire a new teacher for the course." ;
    qDebug() << "Enter a teacher for " << course_name << " [First_name Second_name]: ";
    teacher_name = parse_person_name(get_command());

    int teacher_id = faculty->hire_teacher(teacher_name.first, teacher_name.second);
    faculty->assign_teacher_to_course(teacher_id, course_name);
}

void menu_option_enroll_student(Faculty *faculty)
{
    std::pair<QString, QString> student_name;
    qDebug() << "** Enrolling a new student to " << faculty->get_name() << " **" ;
    qDebug() << "Enter the name of the student to be enrolled: \n > ";
    // Let the faculty create the student

    student_name = parse_person_name(get_command());
    int student_id = faculty->register_student(student_name.first, student_name.second);

    qDebug() << "Which course would you like to enroll to? " ;
    for (size_t course_number = 0; course_number < faculty->get_all_courses().size(); course_number++)
    {
        qDebug() << (course_number + 1) << ": ";
        qDebug() << faculty->get_all_courses()[course_number]->get_course_name() ;
    }
    qDebug() << " > ";
    int course_choice;
    course_choice = get_command().toInt();
    course_choice--;

    QString selected_course = faculty->get_all_courses()[course_choice]->get_course_name();

    faculty->enroll_student_to_course(student_id, selected_course);
    qDebug() << "Student " << faculty->get_student(student_id)->get_name() << " has been enlisted to ";
    qDebug() << faculty->get_all_courses()[course_choice]->get_course_name() ;
}

void menu_option_remove_student(Faculty *faculty)
{
    qDebug() << "The folling students are enrolled: " ;
    int counter = 0;
    auto enlisted_students = faculty->get_all_enlisted_students();
    for (auto student : enlisted_students)
    {
        qDebug() << " |- " << student->get_id() << ": " << student->get_name() ;
        auto enrolled_courses = student->get_enrolled_courses();
    }
    qDebug() << " Select student to remove: " ;
    qDebug() << " > ";
    int selected_student_id = get_command().toInt();
    faculty->remove_student(selected_student_id);
    qDebug() << "Student " << faculty->get_student(selected_student_id) << " removed." ;
}

void modify_faculty(Faculty *faculty)
{
    qDebug() << faculty->get_name() ;
    qDebug() << "-----------------------------------------------" ;
    qDebug() << "You can do the following: " ;
    qDebug() << "Add a new Course (1): " ;
    qDebug() << "Enroll a student (2): " ;
    qDebug() << "Remove a student (3): " ;
    qDebug() << " > ";
    int choice;
    choice = get_command().toInt();
    if (choice == 1)
    {
        menu_option_create_new_course(faculty);
    }
    else if (choice == 2)
    {
        menu_option_enroll_student(faculty);
    }
    else if (choice == 3)
    {
        menu_option_remove_student(faculty);
    }
}

void select_faculty(std::vector<Faculty *> &faculty_list)
{
    qDebug() << "-----------------------------------------------" ;
    qDebug() << "Choose one faculty: " ;
    for (size_t faculty_number = 0; faculty_number < faculty_list.size(); faculty_number++)
    {
        qDebug() << "  " << (faculty_number + 1) << ": " << faculty_list[faculty_number]->get_name() ;
    }
    // Assumes operator is well behaved
    int choice;
    choice = get_command().toInt() - 1;
    Faculty *faculty_to_be_modified = faculty_list[choice];
    modify_faculty(faculty_to_be_modified);
}

void menu_option_view_courses(Faculty *selected_faculty)
{
    qDebug() << "*** COURSES ***" ;
    int counter = 0;
    for (auto course : selected_faculty->get_all_courses())
    {
        int teacher_id = course->get_teacher_by_id();
        QString teacher_name = selected_faculty->get_teacher(teacher_id)->get_name();
        counter++;
        qDebug() << " " << counter << "  | " << course->get_course_name() ;
        qDebug() << "    |  Teacher:" ;
        qDebug() << "    |-  " << teacher_name ;
        qDebug() << "    |  Students: " ;
        for (auto student_id : course->get_enrolled_students())
        {
            QString student_name = selected_faculty->get_student(student_id)->get_name();
            qDebug() << "    |-  " << student_name ;
        }
    }
}

void menu_option_view_teachers(Faculty *selected_faculty)
{
    qDebug() << "*** EMPLOYEES ***" ;
    int counter = 0;
    for (auto employee : selected_faculty->get_all_employees())
    {
        counter++;
        qDebug() << " " << counter << " | " << employee->get_name() ;
    }
}

void browse_faculties(std::vector<Faculty *> &faculties)
{
    qDebug() << "-----------------------" ;
    qDebug() << "Browsing the faculties:" ;
    int counter = 0;
    for (auto faculty : faculties)
    {
        counter++;
        qDebug() << counter << ": " << faculty->get_name() ;
    }
    qDebug() << "Select Faculty: ";
    int select;
    select = get_command().toInt();
    auto selected_faculty = faculties[select - 1];
    qDebug() << "Selected Faculty: " << selected_faculty->get_name() ;
    qDebug() << "1: To view courses" ;
    qDebug() << "2: To view teachers" ;
    select = get_command().toInt();
    if (select == 1)
    {
        menu_option_view_courses(selected_faculty);
    }
    else if (select == 2)
    {
        menu_option_view_teachers(selected_faculty);
    }
}

// Not yet implemented
QString tweek_faculties_menu(std::vector<Faculty *> &faculties)
{
    QString option;
    qDebug() << "Menu: " ;
    qDebug() << "q: To quit press (q)" ;
    qDebug() << "1: To add another Faculty: " ;
    qDebug() << "2: To modify an existing Faculty: " ;
    qDebug() << "3: To browse the Faculties: " ;
    qDebug() << " > ";
    option = get_command();
    // std::cin >> option;
    if (option.compare("q") == 0)
        return option;
    if (option.compare("1") == 0)
        menu_option_create_faculty(faculties);
    else if (option.compare("2") == 0)
        select_faculty(faculties);
    else if (option.compare("3") == 0)
        browse_faculties(faculties);
    return " ";
}

QString welcome_message()
{
    QString message =
        "#######################################\n";
    message += "#                                     #\n";
    message += "#  Welcome to the Education System    #\n";
    message += "#                                     #\n";
    message += "#######################################";

    return message;
}

// Menu
void menu(std::vector<Faculty *> &faculty_list)
{
    qDebug() << welcome_message() ;
    // Check to see if previous data is saved
    std::ifstream f("../data/data_file.txt");
    if (f.good())
    {
        // Load data
        faculty_list = load_save_data();

        qDebug() << "Do you want to display the data (y/n)? ";
        QString choice = get_command();
        if (choice == "y")
        {
            for (auto fac : faculty_list)
            {
                qDebug() << fac->describe_faculty() ;
            }
        }
        QString quit = " ";
        while (quit.compare("q") != 0)
        {
            quit = tweek_faculties_menu(faculty_list);
        }
    }
    else
    {
        // If no data was found, prompt the user to start new
        menu_option_create_faculty(faculty_list);
    }
    // Save the data at the end after a prompt
    qDebug() << "Do you want to save the changes before you quit (yes/no)? ";
    QString answer;
    answer = get_command();
    // std::cin >> answer;
    qDebug() << "###################" ;
    for (auto fac : faculty_list)
    {
        qDebug() << fac->describe_faculty() ;
    }
    if (answer.compare("yes") == 0 || answer.compare("y") == 0)
        save_data(faculty_list);
    qDebug() << "Goodbye!" ;
}
